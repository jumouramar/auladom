/* CRIANDO E REMOVENDO ELEMENTOS */

let secao = document.querySelector(".section_mensages")

function enviar_mensagem(){
    let input = document.querySelector(".input_mensages")
    let mensagem = input.value
    let escopo = document.createElement("div")
    let texto = document.createElement("p")
    texto.innerText = input.value
    escopo.append(texto)
    secao.append(escopo)
}

function limpar_mensagem(){
    secao.innerHTML=""
}

let btn_enviar = document.querySelector("#btn_enviar")
btn_enviar.addEventListener("click", ()=>enviar_mensagem())

let btn_limpar = document.querySelector("#btn_limpar")
btn_limpar.addEventListener("click", ()=>limpar_mensagem())
