/* EVENT LISTENER */ // addEventListener

let button = document.querySelector(".button")

button.addEventListener("click", () =>{
    console.log("clicou")
})


let text = document.querySelector(".text")

text.addEventListener("mouseenter", () =>{
    text.innerText = "Entrou"
})

text.addEventListener("mouseleave", () =>{
    text.innerText = "Saiu"
})
