/* MANIPULAÇÃO DE ELEMENTOS */

let section = document.querySelector(".section")
let div = document.createElement("div")
section.append(div)

// add
div.classList.add("box")
div.classList.add("accept")

// remove
div.classList.remove("accept")

// contains - retorna true ou false - verifica se contém uma determinada classe
console.log(div.classList.contains("box"))
console.log(div.classList.contains("accept"))

// toggle - verifica se determinado elemento possui uma classe
// se não - add 
// se sim - remove
div.classList.toggle("accept")

// replace
div.classList.replace("accept", "error")
