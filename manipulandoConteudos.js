/* MANIPULANDO CONTEÚDOS */

let secao = document.querySelector(".secao")

// innerHTML - retorna o html que está dentro da secao
console.log(secao)
console.log(secao.innerHTML)
    // se quisermos editar esse innerHTML
    secao.innerHTML = "<a class='subtitulo'>Novo Subtitulo</a>"
    console.log(secao)

// innerText - retorna o texto dentro da secao
let subtitulo = document.querySelector(".subtitulo")
console.log(subtitulo.innerText)
    // se quisermos editar esse innerText
    subtitulo.innerText = "Título"

// textContent - retorna innerText + script (difícil de usar)

// Atributos
subtitulo.href="index.html"
subtitulo.style.color="red"