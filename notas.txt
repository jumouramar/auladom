DOCUMENT OBJECT MODEL (DOM)

O que é?
Interface de programação que os navegadores utilizam para representar páginas na web.
É um modelo de representação por objetos.

Qual a relação entre JS e DOM?
O DOM representa e estrutura e os componentes da página e o JavaScript acessa e manipula esses conteúdos.

Por que usar?
Alterar os dados de um site sem que seja necessário fazer uma atualização desta página.