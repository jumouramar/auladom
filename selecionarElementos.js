/* SELECIONAR ELEMENTOS */

// getElementById - retorna o primeiro elemento com o id
let secao1 = document.getElementById("secao_de_texto1")
console.log(secao1)

// getElementByClassName - array com todos os elementos que encontar
let paragrafos = document.getElementsByClassName("paragrafo")
console.log(paragrafos)

// querySelector - busca pelo id ou classe
let secao = document.querySelector("#secao_de_texto1")
console.log(secao)

let paragrafo = document.querySelector(".paragrafo")
console.log(paragrafo)

// querySelectorAll - retorna um grupo de elementos
let paragrafo2 = document.querySelectorAll(".paragrafo")
console.log(paragrafo2)