/* SELETORES SECUNDARIOS */

let paragrafo = document.querySelector(".text")
let html = document.querySelector(".html")

// elemento.parentElement & elemento.parentNode
// retorna o elemento pai
console.log(paragrafo.parentElement)
console.log(paragrafo.parentNode)

        // caso não tenha pai
        console.log(paragrafo.parentElement) // retorna nulo
        console.log(paragrafo.parentNode) // retorna todo o documento



let section = document.querySelector(".section") 

// elemento.firstElementChild & elemento.firstChild
// retorna o primeiro elemento filho
console.log(section.firstElementChild) // retorna o primeiro elemento
console.log(section.firstChild) // retorna a primeira coisa

// elemento.lastElementChild
// retorna o ultimo elemento filho
console.log(section.lastElementChild)

// elemento.children
// retorna um HTMLCollection com todos os filhos
console.log(section.children)